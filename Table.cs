﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class Table
{
	class TypeCompare : Comparer<Type>
	{
		public override int Compare(Type x, Type y)
		{
			return x.FullName.CompareTo(y.FullName);
		}
	}

	private static readonly SortedDictionary<Type, object> cache = new SortedDictionary<Type, object>(new TypeCompare());

	protected static IList<T> Load<T>(string path = null)
	{
		if (string.IsNullOrEmpty(path))
		{
			path = "Table/" + typeof(T).Name;
		}
		var asset = GX.ArchiveManager.Load<TextAsset>(path + ".json");
		var ret = GX.Json.Deserialize<IList<T>>(asset.text);
		return ret;
	}

	public static IList<T> Query<T>(string path = null)
	{
		object wr;
		if (cache.TryGetValue(typeof(T), out wr) == false)
			wr = cache[typeof(T)] = Load<T>(path);
		return (IList<T>)wr;
	}
}