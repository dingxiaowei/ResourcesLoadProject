﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GX;

public class TableTest : MonoBehaviour
{
	IEnumerator Start()
	{
		yield return this.StartCoroutine(ArchiveManager.LoadAsync<TextAsset>("Table/WorldLevelBase.json", asset =>
		{
			Debug.Log(asset.text);
		}));

		TextAsset result = null;
		yield return this.LoadAsync<TextAsset>("Table/WorldLevelBase.json", r => result = r);
		Debug.Log(result.text);
	}
}

